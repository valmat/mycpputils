#include "urldecode.h"
#include <sstream>
#include <iomanip>

namespace {
    inline bool _isXDigit(char c) noexcept
    {
        return ('0' <= c && 'F' >= c);
    }
}

namespace MyUtils {

// See: https://github.com/libevent/libevent/blob/68eb526d7b5a276fc767738193902e7f7b2cfed6/http.c#L3344
// evhttp_uridecode
std::string urlDecode(std::string_view src) noexcept
{
    const char *uri = src.data();
    size_t length   = src.size();

    std::string ret;
    ret.reserve(length / 3);

    for (size_t i = 0; i < length; ++i) {
        char c = uri[i];

        switch(c) {
            case '%' : {
                if ( (i + 2) < length && _isXDigit(uri[i+1]) && _isXDigit(uri[i+2]) ) {
                    char tmp[3] = {uri[i+1], uri[i+2], '\0'};
                    i += 2;
                    c = (char)strtol(tmp, nullptr, 16);
                }
                break;
            }
            case '+' : {
                c = ' ';
                break;
            }
        }
        
        ret += c;
    }

    return ret;
}

std::string urlEncode(std::string_view str) noexcept
{
    std::ostringstream escaped;
    escaped.fill('0');
    escaped << std::hex;

    for (auto c : str) {
        // Keep alphanumeric and other accepted characters intact
        if (std::isalnum(c) || c == '-' || c == '_' || c == '.' || c == '~') {
            escaped << c;
        } else {
            // Any other characters are percent-encoded
            escaped << std::uppercase;
            escaped << '%' << std::setw(2) << int((unsigned char) c);
            escaped << std::nouppercase;
        }
    }

    return escaped.str();
}

} // end of namespace MyUtils