#include "str_utils.h"

namespace MyUtils {

std::string pad_right(std::string const& str, size_t s, char c)
{
    if ( str.size() < s )
        return str + std::string(s-str.size(), c);
    else
        return str;
}

std::string pad_left(std::string const& str, size_t s, char c)
{
    if ( str.size() < s )
        return std::string(s-str.size(), c) + str;
    else
        return str;
}

void collapse_space(std::string &str) noexcept
{
    bool space_found = false;
    size_t pos = 0;

    for (size_t i = 0; i < str.size();) {
        if(size_t space_len = is_ext_space(&str[i], str.size() - i); space_len > 0) {
            if (!space_found) {
                str[pos++] = ' ';
                space_found = true;
            }
            i += space_len;
            
        } else {
            str[pos++] = str[i++];
            space_found = false;
        }
    }

    str.resize(pos);
}

size_t is_ext_space(const char* data, size_t len) noexcept
{
    if (len == 0UL) {
        return 0UL;
    }

    // Checking for regular spaces
    if (std::isspace(data[0])) {
        return 1UL;
    }

    if (len < 2UL) {
        return 0UL;
    }

    uint8_t byte1 = static_cast<uint8_t>(data[0]);
    uint8_t byte2 = static_cast<uint8_t>(data[1]);

    // Checking for two-byte spaces (UTF-8)
    if (byte1 == 0xC2 && (
            byte2 == 0x80 || // IDEOGRAPHIC SPACE
            byte2 == 0xA0)   // NO-BREAK SPACE
        )
    {
        return 2UL;
    }

    if (len < 3UL) {
        return 0UL;
    }

    // Checking for three-byte spaces (UTF-8)
    uint8_t byte3 = static_cast<uint8_t>(data[2]);

    if (
        (byte1 == 0xE1 && byte2 == 0x9A && byte3 == 0x80) || // OGHAM SPACE MARK
        (byte1 == 0xE2 && byte2 == 0x80 && (
            byte3 == 0x81 || // EM QUAD
            byte3 == 0x82 || // EM SPACE
            byte3 == 0x83 || // EN SPACE
            byte3 == 0x84 || // THREE-PER-EM SPACE
            byte3 == 0x85 || // FOUR-PER-EM SPACE
            byte3 == 0x86 || // SIX-PER-EM SPACE
            byte3 == 0x87 || // FIGURE SPACE
            byte3 == 0x88 || // PUNCTUATION SPACE
            byte3 == 0x89 || // THIN SPACE
            byte3 == 0x8A || // HAIR SPACE
            byte3 == 0xAF)) || // NARROW NO-BREAK SPACE
        (byte1 == 0xE2 && byte2 == 0x81 && byte3 == 0x9F) || // MEDIUM MATHEMATICAL SPACE
        (byte1 == 0xE3 && byte2 == 0x80 && byte3 == 0x80) || // IDEOGRAPHIC SPACE
        (byte1 == 0xEF && byte2 == 0xBB && byte3 == 0xBF)) { // ZERO WIDTH NO-BREAK SPACE
        return 3UL;
    }

    // If neither a regular nor a multibyte space is found
    return 0UL;
}

// Эта функция принимает в качестве параметра символ Unicode в виде целого числа (int code) и возвращает кодировку UTF-8 этого символа в виде массива из 4 элементов (std::array<char, 4>).
// UTF-8 является переменно-длинной кодировкой, которая может использовать от 1 до 4 байтов для представления символа Unicode. Функция проверяет диапазон значений входного символа и определяет соответствующее представление UTF-8, основанное на следующих правилах:
// 1. Если символ меньше 0x80 (128), он представляется одним байтом и помещается в result[0].
// 2. Если символ меньше 0x800 (2048), он представляется двумя байтами: первый байт начинается с битовой последовательности "110", а второй байт - с "10". Оставшиеся биты кодовой точки размещаются между двумя байтами, и они записываются в result[0] и result[1].
// 3. Если символ меньше 0x10000 (65536), он представляется тремя байтами: первый байт начинается с битовой последовательности "1110", а следующие два байта - с "10". Оставшиеся биты кодовой точки размещаются между трёмя байтами, и они записываются в result[0], result[1] и result[2].
// 4. Если символ больше или равен 0x10000, он представляется четырьмя байтами: первый байт начинается с битовой последовательности "11110", а следующие три байта - с "10". Оставшиеся биты кодовой точки размещаются между четырьмя байтами, и они записываются в result[0], result[1], result[2] и result[3].
// После того как соответствующие байты заполнены, функция возвращает массив результатов (std::array<char, 4>) с представлением UTF-8 символа.
std::array<char, 4> utf8_encode(int code) noexcept
{
    std::array<char, 4> result = {0,0,0,0};
    if (code < 0x80) {
        result[0]= static_cast<char>(code);
    } else if (code < 0x800) {
        result[0]= static_cast<char>(0xC0 | (code >> 6));
        result[1]= static_cast<char>(0x80 | (code & 0x3F));
    } else if (code < 0x10000) {
        result[0]= static_cast<char>(0xE0 | (code >> 12));
        result[1]= static_cast<char>(0x80 | ((code >> 6) & 0x3F));
        result[2]= static_cast<char>(0x80 | (code & 0x3F));
    } else {
        result[0]= static_cast<char>(0xF0 | (code >> 18));
        result[1]= static_cast<char>(0x80 | ((code >> 12) & 0x3F));
        result[2]= static_cast<char>(0x80 | ((code >> 6) & 0x3F));
        result[3]= static_cast<char>(0x80 | (code & 0x3F));
    }
    return result;
}

void reverse_html_codes(std::string& str) noexcept {
    size_t pos = 0;
    while ((pos = str.find("&#", pos)) != std::string::npos) {
        size_t semicolon_pos = str.find(';', pos);
        if (semicolon_pos == std::string::npos) {
            break;
        }

        bool valid_code = true;
        int code = 0;
        bool hex_mode = ((semicolon_pos > pos + 2) && ((str[pos + 2] == 'x') || (str[pos + 2] == 'X')));
        if( (semicolon_pos == pos + 2) || (hex_mode && (semicolon_pos == pos + 3) ) ) {
            pos = semicolon_pos;
            continue;
        }

        size_t start_pos = pos + 2 + (hex_mode ? 1 : 0);

        for (size_t i = start_pos; i < semicolon_pos; ++i) {
            if (hex_mode) {
                if (std::isxdigit(str[i])) {
                    code = code * 16 + (std::isdigit(str[i]) ? str[i] - '0' : (std::tolower(str[i]) - 'a' + 10));
                } else {
                    valid_code = false;
                    break;
                }
            } else {
                if (std::isdigit(str[i])) {
                    code = code * 10 + (str[i] - '0');
                } else {
                    valid_code = false;
                    break;
                }
            }
        }

        if (valid_code) {
            auto chars = utf8_encode(code);
            for (int i = 0; i < 4 && chars[i]; ++i) {
                str[pos++] = static_cast<char>(chars[i]);
            }
            str.erase(pos, semicolon_pos - pos + 1);
        } else {
            pos = semicolon_pos;
        }
    }
}

} // end of namespace MyUtils