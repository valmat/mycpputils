#include "memuse.h"
#include <iostream>
#include <fstream>
#include <unistd.h>
#include <sstream>

namespace MyUtils {

// https://stackoverflow.com/questions/669438/how-to-get-memory-usage-at-runtime-using-c

MemUsage::MemUsage() noexcept
{
    int tSize = 0, resident = 0, share = 0;
    std::ifstream buffer("/proc/self/statm");
    buffer >> tSize >> resident >> share;
    buffer.close();

    long page_size_kb = sysconf(_SC_PAGE_SIZE) / 1024; // in case x86-64 is configured to use 2MB pages
    
    rss    = resident * page_size_kb;
    shared = share * page_size_kb;
}

std::string MemUsage::to_string() const noexcept
{
    // using namespace std::string_literals;
    MemUsage mu;
    // return "RSS: "s + std::to_string((size_t)mu.rss) + "kB; SharedMemory: "s + std::to_string((size_t)mu.shared) + "kB"s;

    std::stringstream ss;
    ss << "RSS: " << (mu.rss / 1024) << "Mb; SharedMemory: " << (mu.shared / 1024) << "Mb";
    return ss.str();    
}

} // end of namespace MyUtils