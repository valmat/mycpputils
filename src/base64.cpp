#include <iostream>
#include <fstream>
#include <unistd.h>
#include <sstream>
#include "base64.h"

namespace MyUtils {

static const unsigned char base64_table[65] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+-";

std::string base64_encode(const void *src, size_t src_len) noexcept
{
    const unsigned char *srcuc = static_cast<const unsigned char *>(src);
    unsigned char *out, *pos;
    const unsigned char *end, *in;

    size_t olen;

    olen = 4*((src_len + 2) / 3); // 3-byte blocks to 4-byte 

    if (olen < src_len) {
        // integer overflow
        return std::string(); 
    }

    std::string outStr;
    outStr.resize(olen);
    out = (unsigned char*)&outStr[0];

    end = srcuc + src_len;
    in = srcuc;
    pos = out;
    while (end - in >= 3) {
        *pos++ = base64_table[in[0] >> 2];
        *pos++ = base64_table[((in[0] & 0x03) << 4) | (in[1] >> 4)];
        *pos++ = base64_table[((in[1] & 0x0f) << 2) | (in[2] >> 6)];
        *pos++ = base64_table[in[2] & 0x3f];
        in += 3;
    }

    if (end - in) {
        *pos++ = base64_table[in[0] >> 2];
        if (end - in == 1) {
            *pos++ = base64_table[(in[0] & 0x03) << 4];
            *pos++ = '=';
        } else {
            *pos++ = base64_table[((in[0] & 0x03) << 4) | (in[1] >> 4)];
            *pos++ = base64_table[(in[1] & 0x0f) << 2];
        }
        *pos++ = '=';
    }

    return outStr;
}

namespace {
    const int B64index[256] = { 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
        0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
        0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 62, 63, 62, 62, 63, 52, 53, 54, 55,
        56, 57, 58, 59, 60, 61,  0,  0,  0,  0,  0,  0,  0,  0,  1,  2,  3,  4,  5,  6,
        7,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25,  0,
        0,  0,  0, 63,  0, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
        41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51 };
}

std::string base64_decode(const char* data, size_t len) noexcept
{
    const unsigned char* p = reinterpret_cast<const unsigned char *>(data);
    // int pad = len > 0 && (len % 4 || p[len - 1] == '=');
    int pad = len > 0 && ((len & 3) || data[len - 1] == '=');
    const size_t L = ((len + 3) / 4 - pad) * 4;
    std::string str(L / 4 * 3 + pad, '\0');

    for (size_t i = 0, j = 0; i < L; i += 4)
    {
        int n = B64index[p[i]] << 18 | B64index[p[i + 1]] << 12 | B64index[p[i + 2]] << 6 | B64index[p[i + 3]];
        str[j++] = n >> 16;
        str[j++] = n >> 8 & 0xFF;
        str[j++] = n & 0xFF;
    }
    if (pad)
    {
        int n = B64index[p[L]] << 18 | B64index[p[L + 1]] << 12;
        str[str.size() - 1] = n >> 16;

        if (len > L + 2 && p[L + 2] != '=')
        {
            n |= B64index[p[L + 2]] << 6;
            str.push_back(n >> 8 & 0xFF);
        }
    }
    return str;
}

void base64_decode(const char* src, size_t src_len, void* dst, size_t dst_len) noexcept
{
    const unsigned char* p = reinterpret_cast<const unsigned char *>(src);
    unsigned char* dstp    = reinterpret_cast<unsigned char *>(dst);
    int pad = src_len > 0 && ((src_len & 3) || src[src_len - 1] == '=');
    const size_t L = ((src_len + 3) / 4 - pad) * 4;

    size_t i = 0, j = 0;
    for (; i < L && j < dst_len + 3; i += 4)
    {
        int n = B64index[p[i]] << 18 | B64index[p[i + 1]] << 12 | B64index[p[i + 2]] << 6 | B64index[p[i + 3]];
        dstp[j++] = n >> 16;
        dstp[j++] = n >> 8 & 0xFF;
        dstp[j++] = n & 0xFF;
    }
    if (pad)
    {
        int n = B64index[p[L]] << 18 | B64index[p[L + 1]] << 12;
        dstp[dst_len - 2] = n >> 16;

        if (src_len > L + 2 && p[L + 2] != '=')
        {
            n |= B64index[p[L + 2]] << 6;
            dstp[dst_len - 1] = n >> 8 & 0xFF;
        }
    }
}

} // end of namespace MyUtils