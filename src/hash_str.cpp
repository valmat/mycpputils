#include <iostream>
#include "hash_str.h"

namespace MyUtils {

static constexpr size_t b64t_size = 64;
static const char base64_table[b64t_size + 1] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_-";

std::array<char, 11UL> hash_str(const char* data, size_t len) noexcept
{
    uint64_t hs = std::hash<std::string_view>{}( std::string_view(data, len) );
    uint8_t* src = reinterpret_cast<uint8_t*>(&hs);
    std::array<char, 11UL> arr;

    char *pos = arr.data();
    const uint8_t *in = src, *end = src + 8;

    while (end - in >= 3) {
        *pos++ = base64_table[in[0] >> 2UL];
        *pos++ = base64_table[((in[0] & 0x03) << 4UL) | (in[1] >> 4UL)];
        *pos++ = base64_table[((in[1] & 0x0f) << 2UL) | (in[2] >> 6UL)];
        *pos++ = base64_table[in[2] & 0x3f];

        in += 3u;
    }
    *pos++ = base64_table[in[0] >> 2];
    *pos++ = base64_table[((in[0] & 0x03) << 4) | (in[1] >> 4)];
    *pos++ = base64_table[(in[1] & 0x0f) << 2];
        
    return arr;
}

static bool is_valid(char c) noexcept
{
    for (size_t i = 0; i < b64t_size; ++i) {
        if(base64_table[i] == c) {
            return true;
        }
    }
    return false;
}

bool is_hash_str(const char* data, size_t len) noexcept
{
    if( std::tuple_size<decltype(hash_str(data,len))>() != len ) {
        return false;
    }
    for (size_t i = 0; i < len; ++i) {
        if(!is_valid(data[i])) {
            return false;
        }
    }
    return true;
}

} // end of namespace MyUtils