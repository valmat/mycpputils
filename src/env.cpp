#include "env.h"
#include <cctype>    // For std::toupper
#include <locale>    // For std::toupper
#include <cstdlib>   // For std::getenv

namespace MyUtils {

const char* get_env(std::string_view key) noexcept
{
    constexpr static size_t ENV_KEY_LEN = 128;
    char env_key[ENV_KEY_LEN];
    size_t i = 0;
    for (; i < key.length() && i < ENV_KEY_LEN - 1; ++i) {
        env_key[i] = std::toupper(key[i]);
    }
    env_key[i] = '\0';
    return std::getenv(env_key);
}

bool check_set_env(const char* key, std::string& value) noexcept
{
    const char* env_value = std::getenv(key);
    if (!env_value) {
        return false;
    }
    value = env_value;
    return true;
}

} // end of namespace MyUtils
