#pragma once

#include <string>
#include <cstring>
#include <array>

namespace MyUtils {

    template <class T>
    constexpr std::string_view type_name()
    {
        #ifdef __clang__
            std::string_view p = __PRETTY_FUNCTION__;
            return std::string_view(p.data() + 34, p.size() - 34 - 1);
        #elif defined(__GNUC__)
            std::string_view p = __PRETTY_FUNCTION__;
        #  if __cplusplus < 201402
            return std::string_view(p.data() + 36, p.size() - 36 - 1);
        #  else
            return std::string_view(p.data() + 49, p.find(';', 49) - 49);
        #  endif
        #elif defined(_MSC_VER)
            std::string_view p = __FUNCSIG__;
            return std::string_view(p.data() + 84, p.size() - 84 - 7);
        #endif
    }

    std::string pad_right(std::string const& str, size_t s, char c = ' ');
    std::string pad_left(std::string const& str, size_t s, char c = ' ');

    template<const char* beg_tag, const char* end_tag>
    void remove_tags_inplace(std::string& input) noexcept
    {
        size_t pos = 0;
        size_t inputLength = input.length();
        size_t endTagLength = std::strlen(end_tag);

        while (pos < inputLength) {
            size_t tagStart = input.find(beg_tag, pos);
            if (tagStart == std::string::npos) {
                break;
            }

            size_t tagEnd = input.find(end_tag, tagStart);
            if (tagEnd == std::string::npos) {
                input.erase(tagStart);
                break;
            }

            input[tagStart] = ' ';
            input.erase(tagStart + 1, tagEnd + endTagLength - tagStart - 1);
            pos = tagStart;
        }
    }

    template<const char* beg_tag, const char* end_tag>
    std::string remove_tags(const std::string& input) noexcept
    {
        std::string result;
        size_t pos = 0;
        size_t inputLength = input.length();
        size_t endTagLength = std::strlen(end_tag);

        while (pos < inputLength) {
            size_t tagStart = input.find(beg_tag, pos);
            if (tagStart == std::string::npos) {
                result.append(input.substr(pos));
                return result;
            }
            result.append(input.substr(pos, tagStart - pos));

            size_t tagEnd = input.find(end_tag, tagStart);
            if (tagEnd == std::string::npos) {
                return result;
            }
            pos = tagEnd + endTagLength;
        }

        return result;
    }

    // returns number of space bytes or 0
    size_t is_ext_space(const char* data, size_t len) noexcept;

    void collapse_space(std::string &str) noexcept;

    void reverse_html_special_chars(std::string& str) noexcept;
    void reverse_html_codes(std::string& str) noexcept;
    std::array<char, 4> utf8_encode(int code) noexcept;

} // end of namespace MyUtils