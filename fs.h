#pragma once

#if __has_include(<filesystem>)
    #include <filesystem>
    namespace fs = std::filesystem;

#elif __has_include(<experimental/filesystem>)
    #include <experimental/filesystem>
    namespace fs = std::experimental::filesystem;
#else
    static_assert(0, "inclusion 'filesystem' unavailable")
#endif