#pragma once
#include <string>

namespace MyUtils {

    struct MemUsage
    {
        double rss, shared;
        MemUsage() noexcept;
        std::string to_string() const noexcept;
    };

} // end of namespace MyUtils