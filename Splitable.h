 /**
 *  Splitable.h
 *
 *  @author valmat <ufabiz@gmail.com>
 *  @repo   https://bitbucket.org/valmat/mycpputils
 */
 
#pragma once

#include "SplitIterator.h"


namespace MyUtils {

    template<typename StrType, typename BaseStrType = std::string>
    class Splitable
    {
    public:
        typedef SplitIterator<StrType> iterator;

        Splitable(const BaseStrType &str, const char delim, size_t limit = 0) noexcept
            :
            _str(str),
            _limit(limit),
            _delim(delim)
        {}

        iterator begin() const noexcept
        {
            return SplitIterator<StrType>(_str.data(), _str.size(), _limit, _delim);
        }

        iterator end() const noexcept
        {
            return SplitIterator<StrType>(_str.data(), _str.size(), _limit, _delim, true);
        }

        size_t size() const noexcept
        {
            size_t s = 0;
            for(auto && __attribute__((__unused__)) n: *this) {
                ++s;
            }
            return s;
        }

    private:
        BaseStrType _str;
        size_t _limit;
        const char _delim; 
    };

    template<typename StrType = std::string>
    inline Splitable<StrType, std::string> toSplit(const std::string &str, const char delim, size_t limit = 0) noexcept
    {
        return Splitable<StrType>(str, delim, limit);
    }
    template<typename StrType = std::string_view>
    inline Splitable<StrType, std::string_view> toSplit(std::string_view str, const char delim, size_t limit = 0) noexcept
    {
        return Splitable<StrType, std::string_view>(str, delim, limit);
    }    


} // end of namespace MyUtils