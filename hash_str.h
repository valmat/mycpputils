#pragma once
#include <string>
#include <array>

namespace MyUtils {

[[nodiscard]]
std::array<char, 11UL> hash_str(const char* data, size_t len) noexcept;

[[nodiscard]]
bool is_hash_str(const char* data, size_t len) noexcept;

} // end of namespace MyUtils