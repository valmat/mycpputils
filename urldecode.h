#pragma once
#include <string>

namespace MyUtils {

    std::string urlDecode(std::string_view src) noexcept;
    std::string urlEncode(std::string_view str) noexcept;

} // end of namespace MyUtils