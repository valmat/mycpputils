/**
 *  Logger.cpp
 *
 *  @author valmat <ufabiz@gmail.com>
 *  @github https://bitbucket.org/valmat/mycpputils/
 */

#include "Logger.h"
 
namespace MyUtils::Logger {

    std::ofstream  Logger::_file;
    Level          Logger::_level        = Level::debug;
    std::ostream*  Logger::_log          = &std::cerr;

    // Initialize logging with log level and log file
    void Logger::init(Level log_level, const std::string &filename)
    {
        Logger::init(log_level);
        Logger::init(filename);
    }
    // Initialize logging with log level
    void Logger::init(Level log_level)
    {
        Logger::_level = log_level;
    }

    // Initialize logging with log file
    void Logger::init(const std::string &filename)
    {
        _file.open(filename, std::ios_base::app);
        if (_file.is_open()) {
            _file.close();
        }
        _file.open(filename, std::ios_base::app);
        if(_file.is_open()) {
            Logger::_log = &_file;
        } else {
            std::cerr << "Can't open file " << filename << std::endl;
        }
    }
    // Reset log out to stderr
    void Logger::init_stderr()
    {
        Logger::_log = &std::cerr;
        if (_file.is_open()) {
            _file.close();
        }
    }

    // Выводит начало лога и 
    // проверяет нужно ли вообще выводить лог при текущих настройках
    bool Logger::startWriteLog(Level level)
    {
        // Filtering errors by level
        int severity = static_cast<int>(level); 
        if( severity < static_cast<int>(_level) ) {
            return false;
        }

        const char *severityName;
        switch (severity) {
            case static_cast<int>(Level::debug):
                severityName = "dbg";
                break;
            case static_cast<int>(Level::msg):
                severityName = "msg";
                break;
            case static_cast<int>(Level::warn):
                severityName = "wrn";
                break;
            case static_cast<int>(Level::error):
                severityName = "err";
                break;
            default:
                severityName = "???";
                break;
        }

        // see http://www.cplusplus.com/reference/ctime/tm/
        time_t now = time(NULL);
        auto t = localtime(&now);

        (*Logger::_log) 
                << 1900+t->tm_year << "/" 
                << t->tm_mon+1     << "/" 
                << t->tm_mday      << " " 
                << t->tm_hour      << ":" 
                << t->tm_min       << ":" 
                << t->tm_sec 
                << "\t[" << severityName << "] ";

        return true;
    }
}
