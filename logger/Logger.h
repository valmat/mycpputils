/**
 *  Logger.h
 *
 *  @author valmat <ufabiz@gmail.com>
 *  @github https://bitbucket.org/valmat/mycpputils/
 */

#pragma once

#include <iostream>
#include <fstream>

namespace MyUtils::Logger {

    // none | debug | msg |  warn | error | fatal 
    enum class Level : int {
        debug  = 0,
        msg    = 1,
        warn   = 2,
        error  = 3,
        fatal  = 4,
        none   = 5
    };

    struct Logger
    {
    private:
        static Level         _level;
        static std::ofstream _file;
        static std::ostream* _log;

    public: 

        // Initialize logging with log level and log file
        static void init(Level log_level, const std::string &filename);
        // Initialize logging with log level
        static void init(Level log_level);
        // Initialize logging with log file
        static void init(const std::string &filename);
        // Reset log out to stderr
        static void init_stderr();


        template <typename Arg0, typename ...Args>
        void static writeLog(Level level, Arg0&& arg0, Args&&... args)
        {
            if(startWriteLog(level)) {
                writeMsg(std::forward<Arg0>(arg0), std::forward<Args>(args)...);
                (*_log) << std::endl;
            }
        }

        template <typename Arg0, typename ...Args>
        void static debug (Arg0&& arg0, Args&&... args)
        {
            writeLog(Level::debug, std::forward<Arg0>(arg0), std::forward<Args>(args)...);
        }
        template <typename Arg0, typename ...Args>
        void static msg   (Arg0&& arg0, Args&&... args)
        {
            writeLog(Level::msg,   std::forward<Arg0>(arg0), std::forward<Args>(args)...);
        }
        template <typename Arg0, typename ...Args>
        void static warn  (Arg0&& arg0, Args&&... args)
        {
            writeLog(Level::warn,  std::forward<Arg0>(arg0), std::forward<Args>(args)...);
        }
        template <typename Arg0, typename ...Args>
        void static error (Arg0&& arg0, Args&&... args)
        {
            writeLog(Level::error, std::forward<Arg0>(arg0), std::forward<Args>(args)...);
        }


    private:
        static bool startWriteLog(Level level);

        constexpr static void writeMsg() {}

        template <typename Arg0>
        constexpr static void writeMsg(Arg0&& arg0)
        {
            (*_log) << std::forward<Arg0>(arg0);
        }

        template <typename Arg0, typename ...Args>
        constexpr static void writeMsg(Arg0&& arg0, Args&&... args)
        {
            writeMsg(std::forward<Arg0>(arg0));
            writeMsg(std::forward<Args>(args)...);
        }
    };
}

// // How to use:
// using namespace MyUtils::Logger;
// using log = MyUtils::Logger::Logger;
// 
// int main() {
//     // Init logger
//     // Default log to stderr with debug level
//     Logger::init("file_to_log.log");
//     // or
//     Logger::init("file_to_log.log", Level::msg);
//     // or
//     Logger::init(Level::msg);
// 
//     Logger::debug ("debug ", "msg", 55);
//     Logger::msg   ("msg   ", "msg", 55);
//     Logger::warn  ("warn  ", "msg", 55);
//     Logger::error ("error ", "msg", 55);
// 
//     // or log to stderr
//     Logger::init_stderr();
// 
//     Logger::debug ("debug ", "msg", 55);
//     Logger::msg   ("msg   ", "msg", 55);
//     Logger::warn  ("warn  ", "msg", 55);
//     Logger::error ("error ", "msg", 55);
// 
//     foo();
//     bar();
//     Logger::init_stderr();
//     foo();
//     bar();
// 
//     Logger::init(Level::warn);
// 
//     foo();
//     bar();
//     // turn off logger
//     Logger::init(Level::none);
//     foo();
//     bar();
// 
//     return 0;
// }
