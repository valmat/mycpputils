#pragma once
#include <string>

namespace MyUtils {

std::string base64_encode(const void* src, size_t len) noexcept;
std::string base64_decode(const char* data, size_t len) noexcept;
void base64_decode(const char* src, size_t src_len, void* dst, size_t dst_len) noexcept;

template<size_t in_len>
constexpr static size_t base64_encode_out_len = 4 * ((in_len + 2) / 3);

} // end of namespace MyUtils