/**
 *  ct_str.h
 *  Compile time string
 *
 *  @author valmat <ufabiz@gmail.com>
 *  @repo   https://bitbucket.org/valmat/mycpputils
 */

#pragma once


// Compile time strings
template<char ... Chars>
struct __ct_string {
    static constexpr const char value[sizeof...(Chars)+1] = {Chars...,'\0'};
};

template<typename CharT, CharT ...String>
constexpr auto operator"" _cts()
{
    return __ct_string<String...>::value;
}

// Usage:
// template<const char* str>
// struct X {};
// using Y = X<"text"_cts>;
