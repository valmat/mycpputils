#pragma once
#include <string>

namespace MyUtils {

    const char* get_env(std::string_view key) noexcept;
    bool check_set_env(const char* key, std::string& value) noexcept;
 
} // end of namespace MyUtils
