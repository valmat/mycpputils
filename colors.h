#pragma once

//
// Раскраска консоли
// See: https://habrahabr.ru/post/119436/
//

namespace colors {

    constexpr const char* reset   = "\033[0m";    // все атрибуты по умолчанию
    constexpr const char* b       = "\033[1m";    // жирный шрифт (интенсивный цвет)
    constexpr const char* sb      = "\033[2m";    // (semi-bright) полуяркий цвет (тёмно-серый, независимо от цвета)
    constexpr const char* u       = "\033[4m";    // подчеркивание
    constexpr const char* blink   = "\033[5m";    // мигающий
    constexpr const char* rev     = "\033[7m";    // реверсия (знаки приобретают цвет фона, а фон -- цвет знаков)


    constexpr const char* norm    = "\033[22m";    // установить нормальную интенсивность
    constexpr const char* uu      = "\033[24m";    // отменить подчеркивание
    constexpr const char* ublink  = "\033[25m";    // отменить мигание
    constexpr const char* urev    = "\033[27m";    // отменить реверсию


    constexpr const char* red     = "\033[31m";    // красный цвет знаков
    constexpr const char* green   = "\033[32m";    // зелёный цвет знаков
    constexpr const char* yellow  = "\033[33m";    // желтый цвет знаков
    constexpr const char* blue    = "\033[34m";    // синий цвет знаков
    constexpr const char* magenta = "\033[35m";    // фиолетовый цвет знаков
    constexpr const char* cyan    = "\033[36m";    // цвет морской волны знаков
    constexpr const char* gray    = "\033[37m";    // серый цвет знаков
    constexpr const char* grey    = gray;          // серый цвет знаков

    namespace background {
        constexpr const char* black   = "\033[40m";    // чёрный цвет фона
        constexpr const char* red     = "\033[41m";    // красный цвет фона
        constexpr const char* green   = "\033[42m";    // зелёный цвет фона
        constexpr const char* yellow  = "\033[43m";    // желтый цвет фона
        constexpr const char* blue    = "\033[44m";    // синий цвет фона
        constexpr const char* magenta = "\033[45m";    // фиолетовый цвет фона
        constexpr const char* cyan    = "\033[46m";    // цвет морской волны фона
        constexpr const char* grey    = "\033[47m";    // серый цвет фона
    }

    namespace bg = background;

}


