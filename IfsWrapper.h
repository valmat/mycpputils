/**
 *  Обёртка над ifstream 
 *  Для упрощения некоторых действий
 *
 *  @author valmat <ufabiz@gmail.com>
 *  @repo   https://bitbucket.org/valmat/mycpputils
 */

#pragma once

#include <iostream>
#include <fstream>
#include <iterator>

namespace MyUtils {


    // Helper class for line by line iteration
    class IfsLine : public std::string {};
    inline std::istream &operator >> (std::istream &lhs, IfsLine &rhs)
    {
        std::getline(lhs, rhs);
        return lhs;
    }

    // ifstream wrapper
    // Implements line by line iteration, fully read file to string and some else
    class IfsWrapper
    {
    public:
        IfsWrapper(const std::string& filename, std::ios_base::openmode mode = std::ios::in | std::ios::binary) :
            _file(filename, mode)
        {}
        IfsWrapper(const char* filename, std::ios_base::openmode mode = std::ios::in | std::ios::binary) :
            _file(filename, mode)
        {}
        
        IfsWrapper(std::ifstream && file) :
            _file(std::move(file))
        {}


        // Not copyable
        IfsWrapper(const IfsWrapper &) = delete;
        // Movable
        IfsWrapper(IfsWrapper && rhs) :
            _file(std::move(rhs._file))
        {}

        // Читает файл целиком в строку
        std::string readAll()
        {
            std::string result;
            result.assign( (std::istreambuf_iterator<char>(_file) ), (std::istreambuf_iterator<char>() ) );
            return result;
        }

        operator const std::ifstream & () const
        {
            return _file;
        }
        operator std::ifstream & ()
        {
            return _file;
        }

        const std::ifstream & get() const
        {
            return _file;
        }
        std::ifstream & get()
        {
            return _file;
        }

        // Check if file is open
        operator bool ()
        {
            return _file.is_open();
        }
        bool is_open()
        {
            return _file.is_open();
        }

        // Close file
        auto close()
        {
            return _file.close();
        }



        // Retrive line by line iterator
        std::istream_iterator<IfsLine> begin()
        {
            return std::istream_iterator<IfsLine>(_file);
        }
        std::istream_iterator<IfsLine> end()
        {
            return std::istream_iterator<IfsLine>();
        }

        // retrive next line
        std::string getLine()
        {
            std::string line;
            std::getline(_file, line);
            return line;
        }

        size_t size()
        {
            size_t s = 0;
            for(auto && n __attribute__((__unused__)): *this) {
                ++s;
            }
            return s;
        }

        // Check if EOF is reached
        bool hasEof()
        {
            return _file.eof();
        }
        // Returns true if none of the stream's error state flags (eofbit, failbit and badbit) is set.
        bool isValid()
        {
            return _file.good();
        }

        //Reset the file cursor to begin
        [[deprecated("resetCursor")]]
        void reset_cursor()
        {
            _file.clear();
            _file.seekg(0);
        }

        void resetCursor()
        {
            _file.clear();
            _file.seekg(0);
        }

    private:

        std::ifstream _file;
    };

}     
