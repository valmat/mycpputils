/**
 *  Обёртка над istream & ifstream 
 *  Для упрощения некоторых действий
 *
 *  @author valmat <ufabiz@gmail.com>
 *  @repo   https://bitbucket.org/valmat/mycpputils
 */

#pragma once

#include <iostream>
#include <fstream>
#include <iterator>

namespace MyUtils {


    // Helper class for line by line iteration
    class IstreamLine : public std::string {};
    inline std::istream &operator >> (std::istream &lhs, IstreamLine &rhs)
    {
        std::getline(lhs, rhs);
        return lhs;
    }

    // ifstream wrapper
    // Implements line by line iteration, fully read file to string and some else
    class IstreamWrapper
    {
    public:
        IstreamWrapper(const std::string& filename, std::ios_base::openmode mode = std::ios::in | std::ios::binary) :
            _file(filename, mode), _istream(_file)
        {}
        IstreamWrapper(const char* filename, std::ios_base::openmode mode = std::ios::in | std::ios::binary) :
            _file(filename, mode), _istream(_file)
        {}
        
        IstreamWrapper(std::ifstream && file) :
            _file(std::move(file)), _istream(_file)
        {}

        IstreamWrapper() = default;

        // Not copyable
        IstreamWrapper(const IstreamWrapper &) = delete;
        // Movable
        IstreamWrapper(IstreamWrapper && rhs) = default;

        // Читает файл целиком в строку
        std::string readAll()
        {
            std::string result;
            result.assign( (std::istreambuf_iterator<char>(_istream) ), (std::istreambuf_iterator<char>() ) );
            return result;
        }

        operator const std::ifstream & () const
        {
            return _file;
        }
        operator std::ifstream & ()
        {
            return _file;
        }

        operator const std::istream & () const
        {
            return _istream;
        }
        operator std::istream & ()
        {
            return _istream;
        }

        const std::istream & get() const
        {
            return _istream;
        }
        std::istream & get()
        {
            return _istream;
        }

        const std::ifstream & getf() const
        {
            return _file;
        }
        std::ifstream & getf()
        {
            return _file;
        }        

        // Check if file is open
        operator bool ()
        {
            return _file.is_open();
        }
        bool is_open()
        {
            return _file.is_open();
        }

        // Close file
        auto close()
        {
            return _file.close();
        }

        // Retrive line by line iterator
        std::istream_iterator<IstreamLine> begin()
        {
            return std::istream_iterator<IstreamLine>(_istream);
        }
        std::istream_iterator<IstreamLine> end()
        {
            return std::istream_iterator<IstreamLine>();
        }

        // retrive next line
        std::string getLine()
        {
            std::string line;
            std::getline(_istream, line);
            return line;
        }

        size_t size()
        {
            size_t s = 0;
            for(auto && n __attribute__((__unused__)): *this) {
                ++s;
            }
            return s;
        }

        // Check if EOF is reached
        bool hasEof()
        {
            return _istream.eof();
        }
        // Returns true if none of the stream's error state flags (eofbit, failbit and badbit) is set.
        bool isValid()
        {
            return _istream.good();
        }

        void resetCursor()
        {
            _istream.clear();
            _istream.seekg(0);
        }

    private:

        std::ifstream _file;
        std::istream& _istream = std::cin;
    };

}     
