#pragma once

#include <string>
#include <cstring>
#include <algorithm>

namespace MyUtils {

inline
size_t length(const char* str) noexcept
{
    return std::strlen(str);
}

template<size_t N>
constexpr size_t length(char const[N])
{
    return N;
}

template<typename T>
size_t length(T&& t)
{
    return std::forward<T>(t).size();
}

// template<typename T>
// std::string_view sview(T&& t)
// {
//     return std::string_view(std::forward<T>(t));
// }
inline
std::string_view sview(const char* str)
{
    return std::string_view(str, strlen(str));
}
// template<size_t N>
// std::string_view sview(char const str[N])
// {
//     return std::string_view(str, N-1);
// }

template<typename T>
std::string_view sview(T&& t)
{
    return std::string_view(std::forward<T>(t).data(), std::forward<T>(t).size());
}

template<typename... Args>
std::string merge_str(Args&&... args) noexcept
{
    size_t size = (length(args) + ...);

    std::string rez;
    rez.reserve(size);
    ((rez += sview(std::forward<Args>(args))), ...);
    
    return rez;
}

inline
void rm_empty_lines(std::string& str) {
    str.erase(std::unique(str.begin(), str.end(), [](const char& a, const char& b) {
        return (a == '\n' && b == '\n');
    }), str.end());
}

} // end of namespace MyUtils